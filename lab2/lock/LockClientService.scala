package lock

import akka.actor.{Actor, ActorRef, Props}
import akka.event.Logging

// Server --> Client
sealed trait ClientManagementAPI
final case class ServerAcquire(name: String) extends ClientManagementAPI
final case class ServerRelease(name: String) extends ClientManagementAPI
final case class ServerRecallLock(name: String) extends ClientManagementAPI

// Application --> Client
sealed trait AppControlsAPI
case class AppAcquire(name: String) extends AppControlsAPI
case class AppRelease(name: String) extends AppControlsAPI


/**
  * LockClientServers are actors that are the clients communicating between application and server
  *
  * @param myNodeID  sequence number of this actor/server in the app tier
  * @param numNodes  total number of servers in the app tier
  * @param messagesPerBurst number of commands per burst
  */

class LockClientServer(val myNodeID: Int, val numNodes: Int, messagesPerBurst: Int, lServer: ActorRef,
                       leasePeriod: Int, gracePeriod: Int) extends Actor {
    val generator = new scala.util.Random
    val log = Logging(context.system, this)

    var stats = new Stats
    var otherServers: Option[Seq[ActorRef]] = None
    var myLocks: scala.collection.mutable.Set[String] = scala.collection.mutable.Set[String]()
    // Which locks I currently hold
    val lockServer: ActorRef = lServer
    val clientID = myNodeID
    var loadMaster: Option[ActorRef] = None

    // Lock Names
    val baseName = "lock_"
    var curLockNum = 0
    var curReleaseNum = 0

    // Weights
    val targetNumberLocks = 10
    val acquireWeight = 60
    val maxWeight = 100

    val PRINTING = false

    def receive() = {
        // Base actions
        case Prime() =>
            startup()
        case Command() =>
            stats.messages += 1
            handleStatsReporting(sender)
            selectAppCommand();
        case View(e) =>
            otherServers = Some(e)

        // ClientManagementAPI (Server --> Client)
        case ServerAcquire(name) =>
            stats.serverAcq += 1
            handleStatsReporting(sender)
            myLocks += name
            myPrint(s"acquired $name --> myLocks: $myLocks")

            // Go ahead and schedule release
            lockServer ! ClientRelease(name, clientID)
        case ServerRelease(name) =>
            stats.serverRel += 1
            handleStatsReporting(sender)
            myLocks -= name
            myPrint(s"released $name --> myLocks: $myLocks")

        case ServerRecallLock(name) =>
            myPrint("ServerRecallLock: " + name);

        // AppControlsAPI
        case AppAcquire(name) =>
            handleStatsReporting(sender)
            myPrint("AppAcquire: " + name);
        case AppRelease(name) =>
            handleStatsReporting(sender)
            myPrint("AppRelease: " + name);
    }


    // App Commands
    private def acquireLock(name: String): Unit = {
        stats.clientAcq += 1
        myPrint(s"acquiring $name")
//        lockServer ! ClientAcquire(name, clientID)

//        implicit val timeout = TestManager.MAX_WAIT_TIME
        lockServer ! ClientAcquire(name, clientID)
//        val result = Await.result(future, timeout.duration).asInstanceOf[Boolean]
//
//        if(){
//            myLocks += name
//        } else {
//            myPrint(s"Unable to acquire lock $name")
//        }
    }

    private def releaseLock(name: String): Unit = {
        stats.clientRel += 1
        myPrint(s"releasing $name")

        lockServer ! ClientRelease(name, clientID)

//        implicit val timeout = TestManager.MAX_WAIT_TIME
//        val future = lockServer ? ClientRelease(name, clientID)
//
//        if(Await.result(future, timeout.duration).asInstanceOf[Boolean]){
//            myLocks -= name
//        } else {
//            myPrint(s"Unable to release lock $name")
//        }
    }


    /**
      * Decide which lock name to acquire
      */
    private def generateLockName(): String = {
        curLockNum += 1
        baseName + curLockNum.toString
    }


    private def selectAppCommand() = {
        stats.commands += 1
        val commandValue = generator.nextInt(maxWeight)

        // only acquire if you don't have any, otherwise get rid of them
        if (myLocks.isEmpty) { // || commandValue > acquireWeight) {
            // Acquire
            val name = generateLockName()
            acquireLock(name)
        } else {
            // Release, so select a valid lock to release
            val randomLock = myLocks.toVector(generator.nextInt(myLocks.size))
            releaseLock(randomLock)
        }
    }

    private def startup() = {
        // Initialize this node
        myPrint(s"$myNodeID = $this")
    }

    private def handleStatsReporting(master: ActorRef) = {
        if(loadMaster.isEmpty){
            myPrint("setting the master ONCE")
            loadMaster = Option[ActorRef](master)
        }

//        println("#Messages " + stats.messages.toString)

        if (stats.messages >= messagesPerBurst && stats.serverRel >= messagesPerBurst) {
            // Report stats for current command burst

            loadMaster.get ! BurstAck(myNodeID, stats)
            stats = new Stats
        }
    }

    private def myPrint(message: String) = {
//        println(s"client$clientID: $message")
    }
}

object LockClientServer {
    def props(myNodeID: Int, numNodes: Int, burstSize: Int, lServer: ActorRef,
              leasePeriod: Int, gracePeriod: Int): Props = {
        Props(classOf[LockClientServer], myNodeID, numNodes, burstSize, lServer, leasePeriod, gracePeriod)
    }
}
