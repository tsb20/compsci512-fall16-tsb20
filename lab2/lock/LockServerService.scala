package lock

import java.util.concurrent.TimeUnit

import akka.actor.{Actor, ActorSystem, ActorRef, Props, Scheduler}
import akka.event.Logging

import scala.concurrent.duration._



// Client --> Server
sealed trait LockManagementAPI
case class ClientAcquire(name: String, clientID: Int) extends LockManagementAPI
case class ClientRelease(name: String, clientID: Int) extends LockManagementAPI
case class RequestExtension(name: String) extends LockManagementAPI

// Simulator --> Server
sealed trait LockServerControllerAPI
case class GetStats(name: String) extends LockServerControllerAPI
case class RadioSilence(duration: Int) extends LockServerControllerAPI

sealed trait LockTimerAPI
case class LockTimerTimeout(name: String, clientID: Int) extends LockTimerAPI
//case class lockTimerTimeout(name: String, clientID: Int) extends LockTimerAPI

/**
  *
  * @param name Name of the lock in question
  * @param clientID index of the LockClientServer in the list of servers
  * @param client ActorRef to which the lock can later be sent to
  */
class LockEntry(val clientID: Int)


/**
  * LockServer stores the lock lease references and manages requests from Clients to acquire / release locks
  *
  * @param numNodes  total number of servers in the client tier
  * @param messagesPerBurst number of commands per burst
  */

class LockServer(val numNodes: Int, messagesPerBurst: Int, leasePeriod: Int, gracePeriod: Int) extends Actor {
    val log = Logging(context.system, this)
    var stats = new Stats

    // Map lock_str --> Queue of LockClientServer indexes
    val locks = collection.mutable.Map[String, collection.mutable.Queue[LockEntry]]()
    var clients: Option[Seq[ActorRef]] = None
    val system = context.system

    val PRINTING = false

    def receive() = {
        // Receive client ActorRef list
        case View(e) =>
            clients = Some(e)

        // LockManagementAPI Actions
        case ClientAcquire(name, clientID) =>
//            handleStatsReporting(sender)
            handleClientAcquire(name, new LockEntry(clientID))
        case ClientRelease(name, clientID) =>
//            handleStatsReporting(sender)
            handleClientRelease(name, new LockEntry(clientID))
        case RequestExtension(name) =>
            println("RequestExtension: " + name);

        // LockServerControllerAPI
        case GetStats(name) =>
            println("GetStats: " + name);
        case RadioSilence(duration) =>
            println("RequestExtension: " + duration);

        // LockTimerAPI
        case LockTimerTimeout(name, clientID) =>
            lockTimerTimeout(name)
    }


    // LockManagementAPI
    private def handleClientAcquire(name: String, lockEntry: LockEntry) = {
        val lockQueueOption: Option[collection.mutable.Queue[LockEntry]] = locks.get(name)

        // Get the queue or initialize a new one
        val lockQueue = lockQueueOption.getOrElse(collection.mutable.Queue[LockEntry]())
        lockQueue.enqueue(lockEntry)
        locks.update(name, lockQueue)

        // Print queue contents
//        print(s"Enqueued to $name; Queue contents: ")
//        for(l <- lockQueue){
//            print(l.clientID + ", ")
//        }
//        print("\n")

        if(lockQueue.head.clientID == lockEntry.clientID) {
            // Then this client owns the lock and we should tell them
            val cID = lockEntry.clientID
            val c = getClient(lockEntry.clientID)
            myPrint(s"SUCCESS! C$cID acquired $name to $c")
            startLockTimer(name)
            getClient(lockEntry.clientID) ! ServerAcquire(name)
        } else {
            // They are queued, do nothing for now
            val curHeadID = lockQueue.head.clientID
            myPrint(s"$name is queued...and waiting for $curHeadID...")
        }
    }

    private def handleClientRelease(name: String, lockEntry: LockEntry) = {
        val cID = lockEntry.clientID
        val lockQueueOption: Option[collection.mutable.Queue[LockEntry]] = locks.get(name)

        // Get the queue or throw an error
        if(lockQueueOption.isDefined && lockQueueOption.get.nonEmpty){
            val lockQueue = lockQueueOption.get

            // Deque current owner
            if(lockQueue.head.clientID == lockEntry.clientID){
                // is a valid release
                val curOwner = lockQueue.dequeue()
                getClient(curOwner.clientID) ! ServerRelease(name)
                locks.update(name, lockQueue)

                // Print queue contents
//                print(s"Dequeued from $name; Queue contents: ")
//                for(l <- lockQueue){
//                    print(l.clientID + ", ")
//                }
//                print("\n ")

                // Alert next owner if they exist
                if(lockQueue.nonEmpty){
                    val nextOwner = lockQueue.head
                    val nID = nextOwner.clientID
                    myPrint(s"Released the previous owner of $name, now letting C$nID acquire...")
                    startLockTimer(name)
                    getClient(nextOwner.clientID) ! ServerAcquire(name)
                }
            } else {
                // ERROR CASE ?
                val curOwnerID = lockQueue.head.clientID
                val requesterID = lockEntry.clientID
                myPrint(s"Non-owner is attempting to release lock. curOwner: $curOwnerID; requesterID: $requesterID")
            }
        } else {
            // ERROR CASE ?
            myPrint("Release attempted with no holders...")
        }
    }

    private def startLockTimer(name: String) = {
        import system.dispatcher

        // Schedule a lock timer
        system.scheduler.scheduleOnce(leasePeriod milliseconds) {
           self ! lockTimerTimeout(name)
        }
    }

    private def lockTimerTimeout(name: String) = {
        println(s"Timer for $name has timed out...")
    }


    private def startup() = {
        // Initialize this node
        println(s"Lock Server up...")
    }

    private def handleStatsReporting(master: ActorRef) = {
//        if (stats.) {
//
//            // Report stats for current command burst
//            master ! BurstAck(1, stats)
//            stats = new Stats
//        }
    }


    private def myPrint(message: String) = {
//        println(s"LockServer: $message")
    }

    private def getClient(clientID: Int): ActorRef = {
        if (clients.nonEmpty) {
            val realClientList = clients.get
            return realClientList(clientID)
        }

        null
    }
}

object LockServer {
    def props(numNodes: Int, burstSize: Int, leasePeroid: Int, gracePeriod: Int): Props = {
        Props(classOf[LockServer], numNodes, burstSize, leasePeroid, gracePeriod)
    }
}
