package lock

import akka.actor.{ActorSystem, ActorRef, Props}

sealed trait AppServiceAPI
case class Prime() extends AppServiceAPI
case class Command() extends AppServiceAPI
case class View(endpoints: Seq[ActorRef]) extends AppServiceAPI

/**
 * This object instantiates the service tiers and a load-generating master, and
 * links all the actors together by passing around ActorRef references.
 *
 * The service to instantiate is bolted into the KVAppService code.  Modify this
 * object if you want to instantiate a different service.
 */

object LockService {

  def apply(system: ActorSystem, numClients: Int, messagesPerBurst: Int, leasePeriod: Int, gracePeriod: Int): ActorRef = {
    /** Lock-Service Tier */
    val lockServer = system.actorOf(LockServer.props(numClients, messagesPerBurst, leasePeriod, gracePeriod), "LockServer")

    /** Client tier */
    val clients = for(i <- 0 until numClients)
      yield system.actorOf(LockClientServer.props(i, numClients, messagesPerBurst, lockServer, leasePeriod, gracePeriod), "LockClient_" + i)

    /** If you want to initialize a different service instead, that previous line might look like this:
      * yield system.actorOf(GroupServer.props(i, numNodes, stores, ackEach), "GroupServer" + i)
      * For that you need to implement the GroupServer object and the companion actor class.
      * Following the "rings" example.
      */

    /** Tells each server the list of servers and their ActorRefs wrapped in a message. */
    for (client <- clients)
      client ! View(clients)

    // Inform the lockServer of the client list
    lockServer ! View(clients)

    /** Load-generating master */
    val master = system.actorOf(LoadMaster.props(numClients, clients, messagesPerBurst), "LoadMaster")
    master
  }
}
