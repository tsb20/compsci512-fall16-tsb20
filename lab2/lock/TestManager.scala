package lock

import scala.concurrent.duration._
import scala.concurrent.Await

import akka.actor.{Actor, ActorSystem, ActorRef, Props}
import akka.event.Logging
import akka.pattern.ask
import akka.util.Timeout

object TestManager {
  val system = ActorSystem("Rings")
  val numNodes = 3
  val numBursts = 1
  val messagesPerBurst = 5
  val totalPerNode = numBursts * messagesPerBurst

  val leasePeriod = 50
  val gracePeriod = 50

  implicit val MAX_WAIT_TIME = Timeout(10 seconds)


  // Service tier: create app servers and a Seq of per-node Stats
  val master = LockService(system, numNodes, messagesPerBurst, leasePeriod, gracePeriod)

  def main(args: Array[String]): Unit = run()

  def run(): Unit = {
    val s = System.currentTimeMillis
    runUntilDone
    val runtime = System.currentTimeMillis - s
    val throughput = (totalPerNode * numNodes) / runtime
    println(s"Done in $runtime ms ($throughput Kops/sec)")
    system.shutdown()
  }

  def runUntilDone() = {
    master ! Start(totalPerNode)
    val future = ask(master, Join()).mapTo[Stats]
    val done = Await.result(future, MAX_WAIT_TIME.duration)
  }

}
