package lock

class Stats {
  var messages: Int = 0
  var clientAcq: Int = 0
  var clientRel: Int = 0
  var serverAcq: Int = 0
  var serverRel: Int = 0
  var commands: Int = 0
  var errors: Int = 0

  def += (right: Stats): Stats = {
    // total number of commands
    messages += right.messages

    // Group info
    clientAcq += right.clientAcq
    clientRel += right.clientRel
    serverAcq += right.serverAcq

    // Multicast info
    serverRel += right.serverRel
    commands += right.commands

    errors += right.errors
    this
  }

  override def toString(): String = {
    s"Stats msgs=$messages clientAcq=$clientAcq clientRel=$clientRel serverAcq=$serverAcq serverRel=$serverRel commands=$commands err=$errors"
  }
}
