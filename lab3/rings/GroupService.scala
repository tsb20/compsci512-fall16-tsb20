package rings

import akka.actor.{Actor, ActorSystem, ActorRef, Props}
import akka.event.Logging

import scala.collection.mutable.ListBuffer

sealed trait GroupServiceAPI
case class JoinGroup(groupID: BigInt, gs: GroupServer) extends GroupServiceAPI
case class LeaveGroup(groupID: BigInt, gs: GroupServer) extends GroupServiceAPI
case class SendMultiCastGroup(groupID: BigInt, gs: GroupServer) extends GroupServiceAPI
case class ReceiveMultiCast(message: String) extends GroupServiceAPI


// Mutable Seq so we don't have to recreate Seq on every update
class GroupMembership(var membership: ListBuffer[GroupServer])

/**
 * RingService is an example app service for the actor-based KVStore/KVClient.
 * This one stores GroupMembership objects in the KVStore.  Each app server allocates new
 * GroupMemberships (allocCell), writes them, and reads them randomly with consistency
 * checking (touchCell).  The allocCell and touchCell commands use direct reads
 * and writes to bypass the client cache.  Keeps a running set of Stats for each burst.
 *
 * @param myNodeID sequence number of this actor/server in the app tier
 * @param numNodes total number of servers in the app tier
 * @param storeServers the ActorRefs of the KVStore servers
 * @param burstSize number of commands per burst
 */

class GroupServer(val myNodeID: Int, val numNodes: Int, storeServers: Seq[ActorRef], burstSize: Int) extends Actor {
  val generator = new scala.util.Random
  val cellstore = new KVClient(storeServers)
  val dirtycells = new AnyMap
  val localWeight: Int = 70
  val log = Logging(context.system, this)

  var stats = new Stats
  var curMaxGroupID: Int = 0 // Can be increased
  var otherServers: Option[Seq[ActorRef]] = None
  var myGroupMembership: ListBuffer[BigInt] = ListBuffer[BigInt]()

  val PRINTING = false

  // Simulation initial weights
  val maxWeight = 100
  val createNewGroupIWeight = 0 // higher # = more existing group joins, lower # = create more new groups
  val joinIWeight = 100 // higher # = more create/join events
  val leaveIWeight = joinIWeight + 0 // higher # = more group leave events; 100 - leaveWeight = multicast rate

  // Simulation current weights
  var createNewGroupWeight = createNewGroupIWeight
  var joinWeight = joinIWeight
  var leaveWeight = leaveIWeight

  def receive() = {
    // Base actions
      case Prime() =>
        startup()
      case Command() =>
        handleStatsReporting(sender)
        selectAndExecuteCommand()
      case View(e) =>
        otherServers = Some(e)
      // These actions are group forwarding activities to serialize actions to a single group
      case JoinGroup(groupID, gs) =>
        addAnotherGSToGroup(groupID, gs)
      case LeaveGroup(groupID, gs) =>
        removeAnotherGSFromGroup(groupID, gs)
      case SendMultiCastGroup(groupID, gs) =>
        sendMultiCastForAnotherGS(groupID, gs)

        // Receive a message from another GS
      case ReceiveMultiCast(s) =>
        receiveMultiCast(s)
  }

  private def selectAndExecuteCommand() = {
    val sample = generator.nextInt(maxWeight)

    if (sample < joinWeight) {
      joinGroup()
    } else if(sample < leaveWeight){
      leaveGroup()
    } else {
      multicastGroup()
    }
  }

  def receiveMultiCast(multiCastData: String) = {
    stats.multicastsReceived += 1
    myPrint(s"received multicast for G$multiCastData")

    // Check group membership for multi-cast just received
    val groupID = BigInt(multiCastData)
    val groupMembership0 = directRead(groupID)
    if(groupMembership0.nonEmpty){
      val groupMembership = groupMembership0.get
      if(!groupMembership.membership.contains(this)){
        // Received a multi-cast from a group I'm not a part of anymore. Do something?
      }
    }
  }

  // GROUP OWNER GROUP ACTIONS
  def addAnotherGSToGroup(groupID: BigInt, gs: GroupServer): Any = {

    val groupMembersO = directRead(groupID)
    if(groupMembersO.nonEmpty){
      val groupMembers = groupMembersO.get

      // Check that gs is not already a member
      if(!groupMembers.membership.contains(gs)){
        // Add me
        groupMembers.membership += gs
        gs.myGroupMembership += groupID

        val m = groupMembers.membership
        val mgm = gs.myGroupMembership
        val otherID = gs.myNodeID
        myPrint(s"--> [$otherID] joined $groupID. MGM: $mgm, M: $m")

        // store new value
        directWrite(groupID, groupMembers)
      } else {
        // Log that we tried to join a group we are already a member of
        val m = groupMembers.membership
        val mgm = gs.myGroupMembership
        val otherID = gs.myNodeID
        myPrint(s"--> [$otherID] tried to join $groupID again. MGM: $mgm, M: $m")
      }
    } else {
      val otherID = gs.myNodeID
      myPrint(s"--> [$otherID] initializing group $groupID")
      gs.myGroupMembership += groupID
      directWrite(groupID, new GroupMembership(ListBuffer(gs)))

      val groupMembersO = directRead(groupID)
      if(groupMembersO.nonEmpty) {
        //        stats.errors += 1
        val f = groupMembersO.get.membership.head
        val otherID = gs.myNodeID
        myPrint(s"--> [$otherID] post join head: $f")
      }
    }
  }

  def removeAnotherGSFromGroup(groupID: BigInt, gs: GroupServer): Any = {
    val groupMembersO = directRead(groupID)
    if(groupMembersO.nonEmpty){
      val groupMembers = groupMembersO.get

      // check GS is actually a member
      if(groupMembers.membership.contains(gs)){
        // Remove me
        groupMembers.membership -= gs//= groupMembers.membership.filter(_ != gs)
        gs.myGroupMembership -= groupID

        val otherID = gs.myNodeID
        myPrint(s"--> [$otherID] left $groupID")

        // Store new membership
        directWrite(groupID, groupMembers)
      }
    }
  }

  def sendMultiCastForAnotherGS(groupID: BigInt, gs: GroupServer): Any = {
    val groupMembersO = directRead(groupID)
    if(groupMembersO.nonEmpty) {
      val groupMembers = groupMembersO.get

      if(groupMembers.membership.nonEmpty){
        val m = groupMembers.membership

        if(!m.contains(gs)){
          // Sender of multicast is no longer in the group, cancel send
          return
        }

        val mgm = gs.myGroupMembership
        val otherID = gs.myNodeID
        myPrint(s"--> [$otherID] multicasted to G$groupID, MGM: $mgm, M: $m")

        // Send the multicast
        for(groupServer <- groupMembers.membership) {
          groupServer.self ! ReceiveMultiCast(groupID.toString)
        }
      }
    }
  }


  // Group actions

  private def joinGroup(): Any = {
    stats.joins += 1

    val groupID = nextJoinGroupID()

    // Don't try to join a group you are a part of
    if(myGroupMembership.contains(groupID)) return

    // Calculate group owner
    val groupOwner = route(groupID)

    // Send off the message
    groupOwner ! JoinGroup(groupID, this)

    // Maybe add this groupID to MY list of groups
  }

  private def leaveGroup(): Any = {
    stats.leaves += 1

    if(myGroupMembership.isEmpty) return

    val groupID = myGroupMembership(generator.nextInt(myGroupMembership.length))

    myPrint(s"is attempting to leave $groupID. MGM: $myGroupMembership")

    // Calculate group owner
    val groupOwner = route(groupID)

    // Send off the message
    groupOwner ! LeaveGroup(groupID, this)
  }

  private def multicastGroup(): Any = {
    stats.multicasts += 1

    if(myGroupMembership.isEmpty) return


    val groupID = myGroupMembership(generator.nextInt(myGroupMembership.length))

    // Calculate group owner
    val groupOwner = route(groupID)

    // Send off the message
    groupOwner ! SendMultiCastGroup(groupID, this)
  }


  private def nextJoinGroupID(): BigInt ={
    val joinCreateWeight = generator.nextInt(maxWeight)

    if(joinCreateWeight < createNewGroupWeight){
      // join existing group
      BigInt(generator.nextInt(curMaxGroupID + 1))
    } else {
      // create new group
      curMaxGroupID += 1
      myPrint(s"creating new group: $curMaxGroupID")
      BigInt(curMaxGroupID)
    }
  }

  private def startup() = {
    // Initialize this node
    println(s"$myNodeID = $this")
  }

  private def handleStatsReporting(master: ActorRef) = {
    stats.messages += 1

    if (stats.messages >= burstSize) {
      /**
        * "INTERESTING" TEST
        *   STARTING BEHAVIOR: Create and join lots of groups, leave few groups
        *   ENDING BEHAVIOR:   Join / leave equal numbers groups, multicast lots of groups
        */
      updateWeights((100 - createNewGroupIWeight) / TestHarness.numBursts,
                    -1*joinIWeight /  TestHarness.numBursts,
                    -1*(leaveIWeight/2) / TestHarness.numBursts)

      // Report stats for current command burst
      master ! BurstAck(myNodeID, stats)
      stats = new Stats
    }
  }

  private def read(key: BigInt): Option[GroupMembership] = {
    val result = cellstore.read(key)
    if (result.isEmpty) None else
      Some(result.get.asInstanceOf[GroupMembership])
  }

  private def write(key: BigInt, value: GroupMembership, dirtyset: AnyMap): Option[GroupMembership] = {
    val coercedMap: AnyMap = dirtyset.asInstanceOf[AnyMap]
    val result = cellstore.write(key, value, coercedMap)
    if (result.isEmpty) None else
      Some(result.get.asInstanceOf[GroupMembership])
  }

  private def directRead(key: BigInt): Option[GroupMembership] = {
    val result = cellstore.directRead(key)
    if (result.isEmpty) None else
      Some(result.get.asInstanceOf[GroupMembership])
  }

  private def directWrite(key: BigInt, value: GroupMembership): Option[GroupMembership] = {
    val result = cellstore.directWrite(key, value)
    if (result.isEmpty) None else
      Some(result.get.asInstanceOf[GroupMembership])
  }

  private def push(dirtyset: AnyMap) = {
    cellstore.push(dirtyset)
  }

  private def myPrint(message: String) = {
    if(PRINTING){
      println(s"[$myNodeID] " + message)
    }
  }

  /**
    * Modified from KVClient
    * @param groupID
    * @return
    */
  private def route(groupID: BigInt): ActorRef = {
    if(otherServers.nonEmpty){
      val oS = otherServers.get
      val index = (groupID % BigInt(oS.size)).toInt
      return oS((groupID % BigInt(oS.size)).toInt)
    }

    null
  }

  private def updateWeights(newGroupDelta: Int, joinWeightDelta: Int, leaveWeightDelta: Int): Any = {
    createNewGroupWeight += newGroupDelta
    joinWeight += joinWeightDelta
    leaveWeight += leaveWeightDelta
  }


}

object GroupServer {
  def props(myNodeID: Int, numNodes: Int, storeServers: Seq[ActorRef], burstSize: Int): Props = {
    Props(classOf[GroupServer], myNodeID, numNodes, storeServers, burstSize)
  }
}
