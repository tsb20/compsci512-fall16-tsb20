package rings

class Stats {
  var messages: Int = 0
  var groups: Int = 0
  var joins: Int = 0
  var leaves: Int = 0
  var multicasts: Int = 0
  var multicastsReceived: Int = 0
  var errors: Int = 0

  def += (right: Stats): Stats = {
    // total number of commands
    messages += right.messages

    // Group info
    groups += right.groups
    joins += right.joins
    leaves += right.leaves

    // Multicast info
    multicasts += right.multicasts
    multicastsReceived += right.multicastsReceived

    errors += right.errors
	  this
  }

  override def toString(): String = {
    s"Stats msgs=$messages groups=$groups joins=$joins leaves=$leaves multicasts=$multicasts received=$multicastsReceived err=$errors"
  }
}
