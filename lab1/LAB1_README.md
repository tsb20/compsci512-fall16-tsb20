# CS516 Lab 1
The problem of sending messages to groups of servers with populations that are
constantly fluctuating is certainly a difficult task. In particular, it becomes
rather difficult to ensure that actions on particular group happen in some vaguely
serializable order. No two semi-concurrent changes in a group's membership should
every corrupt the other. In order to properly manage this, I opted have each group
"owned" by a particular group server. The same algorithm that is used by `KVClient`
to `route` calls to a particular instance is also employed by `GroupServers` to
handle the calls for a particular group.

I discovered through tracing and experimentation that group broadcasts will not
always appear in the same order that they were originally broadcast in. The
delay between message sending and message receiving is not deterministic, especially
because each `Actor` is running as its own thread. This problem is really only
ever troublesome if a `GroupServer` leaves a Group between the moment when a
multicast is sent to a group, and when that message is actually received.

## Weights
In order for multicasts to be sent to the most number of `GroupServers`, there
needs to be high group joining rates, and low group leaving rates. I additionally defined another weight, `createNewGroupWeight`. Every time that a `GroupServer`
selects to join a group, there is another choice between incrementing the current
group counter (creating a new group), or joining one of the groups that it isn't
already a part of. This allows the number of groups to grow in a reasonably controlled manner. Multicasting is maximized when joining rates are high in the beginning of the experiment, and multicasting rates are high in the end.

## "Interesting" Experiment
My experiment consisted of shifting the weights such that in the beginning of the simulation, many groups are created and joined, with no groups being left. As the simulation continues, `GroupServers` begin to create/join fewer groups and begin multicasting more. The behavior is just as expected, that the number of `GroupServers` that receive  Multicasts is greater than the number of `multicast(groupID)` events that are initiated.

Experiment outputs:
```
Run 1:
Stats msgs=10000 groups=0 joins=5464 leaves=2313 multicasts=2213 received=8018 err=0
Send-to-receive ratio: 0.276

Run 2:
Stats msgs=10000 groups=0 joins=5401 leaves=2206 multicasts=2383 received=10101 err=0
Send-to-receive ratio: 0.236
```

Compared with a balanced, unshifting weight distribution:
```
Run 1:
Stats msgs=10000 groups=0 joins=3341 leaves=3279 multicasts=3370 received=1724 err=0
Send-to-receive ratio: 1.955

Run 2:
Stats msgs=10000 groups=0 joins=3252 leaves=3361 multicasts=3377 received=1140 err=0
Send-to-receive ratio: 2.962
```
